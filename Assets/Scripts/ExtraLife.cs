﻿using UnityEngine;
using System.Collections;

public class ExtraLife : MonoBehaviour {

	public int lifeValue = 1;
	public bool taken = false;
	public GameObject explosion;

	void OnTriggerEnter2D (Collider2D other)
	{
		if ((other.tag == "Player" ) && (!taken) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
		{
			// mark as taken so doesn't get taken multiple times
			taken=true;

			// do the player collect coin thing
			other.gameObject.GetComponent<CharacterController2D>().CollectLife(lifeValue);

			// destroy the coin
			DestroyObject(this.gameObject);
		}
	}
}
