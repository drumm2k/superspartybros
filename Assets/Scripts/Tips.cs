﻿using UnityEngine;
using System.Collections;

public class Tips : MonoBehaviour {

	public GameObject Tip;
	public string playerLayer = "Player";
	public string _tipLayer = "Tip";

	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			CharacterController2D player = collision.gameObject.GetComponent<CharacterController2D>();
			Tip.SetActive(true);
		}
	}
	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			CharacterController2D player = collision.gameObject.GetComponent<CharacterController2D>();
			Tip.SetActive(false);
		}
	}
}
